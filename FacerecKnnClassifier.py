# stores previously generated face embedings to avoid the overhead of generating them every time


from math import sqrt
from sklearn import neighbors
from os import listdir
from os.path import isdir, join, isfile, splitext
import pickle
from PIL import Image, ImageFont, ImageDraw, ImageEnhance
import face_recognition
from face_recognition import face_locations
from face_recognition.cli import image_files_in_folder

class FacerecKnnClassifier:

    def __init__(self):
        self.ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}
        self.knn_clf = None
        self.storedEncodings = {} # filename: {encoding, personName}

    def trainFlat(self, train_dir, n_neighbors = None, knn_algo = 'ball_tree', verbose=False):
        """
        Trains a k-nearest neighbors classifier for face recognition.

        :param train_dir: directory that contains a sub-directory for each known person, with its name.

         (View in source code to see train_dir example tree structure)

         Structure:
            <train_dir>/
               ├── <somename>.1.jpeg
               ├── <somename>.2.jpeg
               ├── <someothername>.1.jpeg
               └── ...
        :param model_save_path: (optional) path to save model of disk
        :param n_neighbors: (optional) number of neighbors to weigh in classification. Chosen automatically if not specified.
        :param knn_algo: (optional) underlying data structure to support knn.default is ball_tree
        :param verbose: verbosity of training
        :return: returns knn classifier that was trained on the given data.
        """
        X = []
        y = []

        for img_path in image_files_in_folder(train_dir):

            # check if we already have an encoding or this is a new image
            entry = self.storedEncodings.get(img_path)
            if entry is not None:
                print("Image found in cache: {}".format(img_path))
                faceEncoding = entry['encoding']
                personName = entry['name']
            else:
                print("Image {} not found in cache".format(img_path))
                image = face_recognition.load_image_file(img_path)
                faces_bboxes = face_locations(image)
                if len(faces_bboxes) != 1:
                    if verbose:
                        print("image {} not fit for training: {}".format(img_path, "didn't find a face" if len(faces_bboxes) < 1 else "found more than one face"))
                    continue
                baseFilename = img_path.split('/')[img_path.count('/')]
                baseFilenameParts = baseFilename.split('.')
                personName = baseFilenameParts[len(baseFilenameParts)-3] # get the part before ".n.jpg"

                if len(baseFilenameParts) < 3:
                    print("image {} does not conform to naming scheme <somename>.n.jpg".format(img_path))
                    continue
                print("Processing image {} ".format(img_path))
                faceEncoding = face_recognition.face_encodings(image, known_face_locations=faces_bboxes)[0]
                self.storedEncodings[img_path] = {'encoding':faceEncoding, 'name':personName}

            X.append(faceEncoding)
            y.append(personName)

        if n_neighbors is None:
            n_neighbors = int(round(sqrt(len(X))))
            print("Chose n_neighbors automatically as:", n_neighbors)

        self.knn_clf = neighbors.KNeighborsClassifier(n_neighbors=n_neighbors, algorithm=knn_algo, weights='distance')
        self.knn_clf.fit(X, y)


        #return knn_clf

    def predict(self, X_img_path, DIST_THRESH = .5):
        """
        recognizes faces in given image, based on a trained knn classifier

        :param X_img_path: path to image to be recognized
        :param knn_clf: (optional) a knn classifier object. if not specified, model_save_path must be specified.
        :param model_save_path: (optional) path to a pickled knn classifier. if not specified, model_save_path must be knn_clf.
        :param DIST_THRESH: (optional) distance threshold in knn classification. the larger it is, the more chance of misclassifying an unknown person to a known one.
        :return: a list of names and face locations for the recognized faces in the image: [(name, bounding box), ...].
            For faces of unrecognized persons, the name 'N/A' will be passed.
        """

        if not isfile(X_img_path) or splitext(X_img_path)[1][1:] not in self.ALLOWED_EXTENSIONS:
            raise Exception("invalid image path: {}".format(X_img_path))

        if self.knn_clf is None:
            return ["N/A(no model)"];
            raise Exception("must supply knn classifier either thourgh knn_clf or model_save_path")

        X_img = face_recognition.load_image_file(X_img_path)
        X_faces_loc = face_locations(X_img)
        if len(X_faces_loc) == 0:
            return []

        faces_encodings = face_recognition.face_encodings(X_img, known_face_locations=X_faces_loc)


        closest_distances = self.knn_clf.kneighbors(faces_encodings, n_neighbors=1)

        is_recognized = [closest_distances[0][i][0] <= DIST_THRESH for i in range(len(X_faces_loc))]

        # predict classes and cull classifications that are not with high confidence
        return [(pred, loc) if rec else ("N/A", loc) for pred, loc, rec in zip(self.knn_clf.predict(faces_encodings), X_faces_loc, is_recognized)]
