import os
import json
import pickle
from face_recognition_knn import train, predict, draw_preds, trainFlat
from flask import Flask, flash, request, redirect, url_for, abort, send_file, render_template
from flask import send_from_directory
from werkzeug.utils import secure_filename
from FacerecKnnClassifier import FacerecKnnClassifier

# import threading
# threading.stack_size(2*1024*1024)

UPLOAD_FOLDER = './uploads'
TRAINING_FOLDER = './known_faces/train-flat'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
MODEL_PATH="saved_models/model"

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['TRAINING_FOLDER'] = TRAINING_FOLDER

classifier = None

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            #predictions = predict(app.config['UPLOAD_FOLDER'] + '/' + filename, uploads_classifier)
            predictions = classifier.predict(app.config['UPLOAD_FOLDER'] + '/' + filename)

            return json.dumps(predictions)
            # return 'prediction: ' + ''.join( preds[0][0] )
            # return 'prediction: ' + pred_to_str(preds[0])
            #return redirect(url_for('uploaded_file', filename=filename))
    # get method
    files = os.listdir(app.config['TRAINING_FOLDER'])
    return render_template('home.html', files=files)

# dirty hack using a GET method to delete stuff, don't do it at home
@app.route('/delete/<file>')
def delete(file):
    filePath = app.config['TRAINING_FOLDER'] + '/' + file;
    if os.path.exists(filePath):
        os.remove(filePath)
    else:
        print("The file does not exist: " + filePath)

     # TODO refresh model - remove this image
    return redirect(url_for('upload_file'))

@app.route('/train', methods=['POST'])
def upload_training_image():
    # check if the post request has the file part
    if 'file' not in request.files:
        flash('No file part')
        return redirect(request.url)
    file = request.files['file']
    # if user does not select file, browser also
    # submit an empty part without filename
    if file.filename == '':
        flash('No selected file')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['TRAINING_FOLDER'], filename))

        classifier.trainFlat(app.config['TRAINING_FOLDER'])
    return redirect(url_for('upload_file'))


@app.route('/uploads/', defaults={'req_path': ''})
@app.route('/uploads/<req_path>')
def dir_listing(req_path):
    BASE_DIR = app.config['TRAINING_FOLDER']

    # Joining the base and the requested path
    abs_path = os.path.join(BASE_DIR, req_path)

    # Return 404 if path doesn't exist
    if not os.path.exists(abs_path):
        return abort(404)

    # Check if path is a file and serve
    if os.path.isfile(abs_path):
        return send_file(abs_path)

    # Show directory contents
    files = os.listdir(abs_path)
    return render_template('folder.html', files=files)

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    # TODO why does this code run twice??
    if classifier is None:
        classifier = FacerecKnnClassifier()
        classifier.trainFlat(TRAINING_FOLDER)
    # create the Flask REST app
    app.run(host='0.0.0.0', port=5000, debug=True)